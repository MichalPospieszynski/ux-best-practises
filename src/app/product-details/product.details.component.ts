import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {ProductsService} from "../shared/service/products.service";
import {Product} from "../model/shop/Product";
import {NgbCarouselConfig} from "@ng-bootstrap/ng-bootstrap";
import {CategoriesService} from "../shared/service/categories.service";
import {Category} from "../model/shop/Category";
import {ProductImage} from "../model/shop/ProductImage";
import {ImagesPair} from "../model/shop/ImagesPair";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {CartService} from "../shared/service/cart.service";
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-product-details, [app-product-details]',
  templateUrl: './product.details.component.html',
  styleUrls: ['./product.details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductDetailsComponent implements OnInit {

  productId: string = null;

  state: {
    breadcrumbs: string[],
    product: Product,
    similarProducts: Product[],
    category: Category,
    images: ImagesPair[],
    colorImages: ProductImage[],
    colors: string[],
    color: string,
    sizes: string[],
    size: string
  };

  constructor(private route: ActivatedRoute,
              private productsService: ProductsService,
              private categoriesService: CategoriesService,
              private cartService: CartService,
              private config: NgbCarouselConfig,
              protected sanitizer: DomSanitizer,
              private snackBar: MatSnackBar,
              private router: Router) {
    config.interval = null;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = true;
    config.wrap = true;
    this.state = {
      breadcrumbs: [],
      product: null,
      similarProducts: [],
      category: null,
      images: [],
      colorImages: [],
      colors: [],
      color: null,
      sizes: [],
      size: null
    }
  }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.resetState();
      window.scrollTo(0, 0)
    });
    this.resetState();
  }

  resetState() {
    this.state.sizes = ['S', 'M', 'L', 'XL'];
    this.state.size = this.state.sizes[0];
    this.productId = this.route.snapshot.paramMap.get('id');
    this.state.product = this.productsService.state.allProducts.find(it => it.id == Number(this.productId));
    this.state.similarProducts = this.productsService.state.allProducts.filter(it => it.id != this.state.product.id && it.categoryId == this.state.product.categoryId).slice(0, 3);
    this.state.category = this.categoriesService.state.mainCategory != null ? this.categoriesService.getCategoryById(this.categoriesService.state.mainCategory) : null;
    this.setColors();
    this.setColorImages();
    this.setImages();
    this.setBreadcrumbs();
  }

  setBreadcrumbs() {
    let bc = [];
    let cat = this.categoriesService.getCategoryById(this.state.product.categoryId);
    bc.push(cat.name);
    if (cat.parent != null) {
      let par = cat.parent;
      bc.push(par.name);
      if (par.parent != null) {
        bc.push(par.parent.name);
      }
    }
    this.state.breadcrumbs = bc.reverse().concat(this.state.product.name);
  }

  private setColor(color: string) {
    this.state.color = color;
    this.setColorImages();
    this.setImages();
  }

  private setColors() {
    this.state.colors = [];
    let colors = this.state.product.images.map(it => it.color);
    colors.forEach(it => {
      if (!this.state.colors.includes(it)) {
        this.state.colors.push(it);
      }
    });
    this.state.colors.sort();
    this.state.color = this.state.colors[0];
  }

  private setColorImages() {
    this.state.colorImages = this.state.product.images.filter(it => it.color == this.state.color);
  }

  private setImages() {
    if (this.state.colorImages.length > 0) {
      this.state.images = this.mapToImagesPair(this.state.product.images.filter(it => it.color == this.state.colorImages[0].color));
    }
  }

  private mapToImagesPair(images: ProductImage[]) {
    let result: ImagesPair[] = [];
    images.forEach((image, index) => {
      let imagesPair: ImagesPair = new ImagesPair();
      imagesPair.primaryImageContent = image.content;
      imagesPair.secondaryImageContent = index == (images.length - 1) ? images[0].content : images[index + 1].content;
      result.push(imagesPair);
    });
    return result;
  }

  private prepareStyle(): SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(this.state.product.style.replace(/, /g, '<br/>'));
  }

  addToCart() {
    this.cartService.addProductToCart(this.state.product, this.state.size);
    this.snackBar.open('Produkt w koszyku');
  }
}



