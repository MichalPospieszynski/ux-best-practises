import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserModalComponent} from "../user/user.component";
import {AuthorizationService} from "../shared/service/authorization.service";
import {CategoriesService} from "../shared/service/categories.service";
import {Category} from "../model/shop/Category";
import {CategoryMapper} from "../shared/mapper/category.mapper";
import {Product} from "../model/shop/Product";
import {ProductsService} from "../shared/service/products.service";
import {Router} from "@angular/router";
import {ProductsFilterModalComponent} from "../products/products-filter/products.filter.component";
import {CartService} from "../shared/service/cart.service";

@Component({
  selector: 'app-header, [app-header]',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, AfterViewChecked{

  @ViewChild("searchInput", null) searchInputField: ElementRef;

  state : {
    categories: Category[],
    hideSearchInput: boolean,
    hideCart: boolean,
    cardVisibility: boolean,
    searchInputValue: string,
    searchedProducts: Product[]
  };

  constructor(private modalService: NgbModal,
              public categoriesService: CategoriesService,
              public productsService: ProductsService,
              public cartService: CartService,
              public authService: AuthorizationService,
              private cdRef:ChangeDetectorRef,
              private router: Router) {
    this.state = {
      categories: null,
      hideSearchInput: true,
      hideCart: true,
      cardVisibility: false,
      searchInputValue: "",
      searchedProducts: []
    }
  }

  ngOnInit() {
    this.categoriesService.prepareCategories()
      .subscribe(categories => {
        this.categoriesService.setCategories(CategoryMapper.map(categories));
        this.state.categories = this.categoriesService.getCategories().sort((o1, o2) => o1.id - o2.id);
      }, error1 => {
        console.log('error status: ' + error1)
      });
  }

  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }

  openUserPanel() {
    this.modalService.open(UserModalComponent, {windowClass: "user-modal"});
  }

  logout() {
    this.authService.email = null;
  }

  showSearchInput() {
    this.state.hideSearchInput = !this.state.hideSearchInput;
    setTimeout(()=>{
      if (!this.state.hideSearchInput) {
        this.searchInputField.nativeElement.focus();
      }
    },0);
  }

  searchInputChanged() {
    var searchedProducts = this.productsService.state.notFilteredProducts.filter(it => it.name.toUpperCase().includes(this.state.searchInputValue.toUpperCase()));
    if (searchedProducts.length > 3) {
      searchedProducts = searchedProducts.slice(0, 3);
    }
    this.state.searchedProducts = searchedProducts;
  }

  resetSearch() {
    this.state.hideSearchInput = true;
    this.state.searchInputValue = "";
    this.state.searchedProducts = [];
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.productsService.state.productsFilter.search = this.state.searchInputValue;
      this.router.navigate(['/products/1']);
      this.productsService.filterChange.next();
      this.resetSearch();
    }
  }

  showCart() {
    this.state.hideCart = false;
  }

  hideCart() {
    this.state.hideCart = true;
  }

  changeCartVisibility() {
    this.state.cardVisibility = !this.state.cardVisibility;
  }

  removeItem(id: number, size: string) {
    this.cartService.state.cart = this.cartService.state.cart.filter(it => it.product.id !== id || it.size !== size);
  }

}
