import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Category} from "../model/shop/Category";
import {CategoriesService} from "../shared/service/categories.service";

@Component({
  selector: 'app-footer, [app-footer]',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

  state : {
    category1: Category,
    category2: Category,
    category3: Category
  };

  constructor(public categoriesService: CategoriesService) {
    this.state = {
      category1: null,
      category2: null,
      category3: null
    }
  }

  ngOnInit(): void {
    this.state.category1 = this.categoriesService.getCategoryById(1);
    this.state.category2 = this.categoriesService.getCategoryById(2);
    this.state.category3 = this.categoriesService.getCategoryById(3);
    this.state.category1.subCategories = this.categoriesService.getSubCategoriesById(1);
    this.state.category2.subCategories = this.categoriesService.getSubCategoriesById(2);
    this.state.category3.subCategories = this.categoriesService.getSubCategoriesById(3);
  }

}
