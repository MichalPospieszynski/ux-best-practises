import {Injectable} from '@angular/core';
import {Category} from "../../model/shop/Category";
import {Product} from "../../model/shop/Product";

@Injectable({
  providedIn: 'root'
})
export class CategoryHelper {

  public getCategory(categories : Category[], categoryId : number): Category{
    let result = categories.find(cat => cat.id == categoryId);
    if(result == null) {
      categories.forEach(category => {
        let cat = category.subCategories.find(c => c.id == categoryId);
        result = cat != null ? cat : result
      });
    }
    return result;
  }

}
