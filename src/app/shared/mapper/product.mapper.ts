import {ProductDTO} from "../../model/dto/ProductDTO";
import {Product} from "../../model/shop/Product";
import {ProductImageDTO} from "../../model/dto/ProductImageDTO";
import {ProductImage} from "../../model/shop/ProductImage";
import {isNullOrUndefined} from "util";

export class ProductMapper {

  static map(products: ProductDTO[]): Product[] {
    return isNullOrUndefined(products) ? [] : products.map(value => this.mapSingle(value));
  }

  static mapSingle(product: ProductDTO): Product {
    return new Product(
      product.id,
      product.name,
      product.style,
      product.fabric,
      product.fashion,
      product.brandLogo,
      product.price,
      this.mapImages(product.images),
      product.categoryId
    );
  }

  static mapImages(productImages: ProductImageDTO[]): ProductImage[] {
    return isNullOrUndefined(productImages) ? [] : productImages.map(value => this.mapImage(value)).sort((a, b) => (a.color+a.order).localeCompare((b.color+b.order)));
  }

  static mapImage(productImage: ProductImageDTO): ProductImage {
    return new ProductImage(
      productImage.order,
      productImage.color,
      productImage.content
    );
  }

}
