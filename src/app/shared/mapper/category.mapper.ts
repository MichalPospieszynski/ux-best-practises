import {Category} from "../../model/shop/Category";
import {CategoryDTO} from "../../model/dto/CategoryDTO";
import {isNullOrUndefined} from "util";

export class CategoryMapper {

  static map(categories: CategoryDTO[]): Category[] {
    let result: Category[] = [];
    categories.forEach( value => result.push(this.mapSingle(value)));
    categories.forEach(value => {
      result.find( it => it.id===value.id).parent = value.parentId!=null ? result.find(it => it.id===value.parentId) : null;
    });
    result.forEach(cat => {
      cat.subCategories = result.filter(it => it.parent === cat);
      cat.subCategories.forEach(sub => {
        sub.subCategories = result.filter(c => c.parent === sub);
      });
    });
    return result.filter(value => value.parent === null);
  }

  static mapSingle(category: CategoryDTO): Category {
    return new Category(category.id, category.name);
  }

}
