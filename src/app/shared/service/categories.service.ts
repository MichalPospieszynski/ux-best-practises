import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CategoryDTO} from "../../model/dto/CategoryDTO";
import {Category} from "../../model/shop/Category";
import {Subject, Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService implements OnDestroy {

  mainCategoryChange = new Subject<number>();
  selectedCategoryChange = new Subject<number>();

  private subscriptions: Subscription[] = [];

  state : {
    mainCategory: number,
    categories: Category[]
  };

  constructor(private httpClient: HttpClient) {
    this.state = {
      mainCategory: null,
      categories: []
    };
    this.subscriptions.push(this.mainCategoryChange.subscribe(value => {
      this.state.mainCategory = value;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  setCategories(categories: Category[]) {
    this.state.categories = categories;
  }

  getCategories(): Category[] {
    return this.state.categories;
  }

  getSubCategories(): Category[] {
    let result: Category[] = [];
    this.state.categories.forEach(value => {
      result = result.concat(value.subCategories.filter(it => it.parent!=null && it.parent.id === this.state.mainCategory));
    });
    result.forEach(value => {
      value.visible = false;
      value.subCategories.forEach(value1 => value1.visible = false);
    });
    return result;
  }

  getSubCategoriesById(id: number): Category[] {
    let result: Category[] = [];
    this.state.categories.forEach(value => {
      result = result.concat(value.subCategories.filter(it => it.parent!=null && it.parent.id === id));
    });
    return result;
  }

  getCategoryById(categoryId: number): Category {
    let result: Category = null;
    this.state.categories.forEach(value => {
      if(value.id === categoryId){
        result = value;
      }
      value.subCategories.forEach(sub => {
        if(sub.id === categoryId) {
          result = sub;
        }
        sub.subCategories.forEach(subsub => {
          if(subsub.id === categoryId) {
            result = subsub;
          }
        })
      });
    });
    return result;
  }

  prepareCategories() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let url = 'http://localhost:8080/categories';
    return this.httpClient.get<CategoryDTO[]>(url, httpOptions);
  }

}
