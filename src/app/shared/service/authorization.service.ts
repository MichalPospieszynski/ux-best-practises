import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Token} from "./token.model";
import {ProductDTO} from "../../model/dto/ProductDTO";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  public email: string = null;

  constructor(private modalService: NgbModal,
              private httpClient: HttpClient) {
  }

  signIn(username: string, password: string) {
    let url = 'http://localhost:8080/oauth/token?grant_type=password&username=' + username + '&password=' + encodeURIComponent(
      password);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("client:123"));


    return this.httpClient.post<Token>(url, null, {headers: headers});

  }

  signUp(username: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let body = {
      username: username,
      password: password
    };

    let url = 'http://localhost:8080/users';
    this.httpClient.post<ProductDTO[]>(url, body, httpOptions)
      .subscribe(response => {
        this.modalService.dismissAll();
      });
  }

  static getToken(): string {
    let token = localStorage.getItem('token');
    if (token == null) {
      return '';
    }
    return token;
  }

}
