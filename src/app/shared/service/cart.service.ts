import {Injectable, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {CartItemDTO} from "../../model/dto/CartItemDTO";
import {ProductDTO} from "../../model/dto/ProductDTO";

@Injectable({
  providedIn: 'root'
})
export class CartService implements OnDestroy {

  private subscriptions: Subscription[] = [];

  state: {
    cart: CartItemDTO[]
  };

  constructor(private httpClient: HttpClient) {
    this.resetState();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  addProductToCart(product: ProductDTO, size: string) {
    if (this.state.cart.find(it => it.product.id == product.id && it.size == size) == undefined) {
      this.state.cart = this.state.cart.concat({product: product, size: size, amount: 1});
    } else {
      this.state.cart.forEach( it => {
        if (it.product.id == product.id && it.size == size) {
          it.amount ++;
        }
      });
    }
  }

  resetState() {
    this.state = {
      cart: []
    };
  }

  getCartSummary(): number {
    let result = 0;
    this.state.cart.forEach(it => {
      result = result + it.amount*it.product.price
    });
    return result;
  }

  getCartItemsCount(): number {
    let result = 0;
    this.state.cart.forEach(it => {
      result = result + it.amount
    });
    return result;
  }

}
