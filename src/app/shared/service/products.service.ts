import {Injectable, OnDestroy} from '@angular/core';
import {Product} from "../../model/shop/Product";
import {Subject, Subscription} from "rxjs";
import {CategoriesService} from "./categories.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ProductDTO} from "../../model/dto/ProductDTO";
import {ProductMapper} from "../mapper/product.mapper";
import {ProductsFilter} from "../../model/shop/ProductsFilter";
import {isNullOrUndefined} from "util";

@Injectable({
  providedIn: 'root'
})
export class ProductsService implements OnDestroy {

  goPage = new Subject<number>();
  sortOptionChange = new Subject<number>();
  filterChange = new Subject<number>();

  private subscriptions: Subscription[] = [];

  state: {
    notFilteredProducts: Product[],
    allProducts: Product[],
    products: Product[],
    productsCount: number,
    maxPage: number,
    productsFilter: ProductsFilter
  };

  constructor(private categoriesService: CategoriesService,
              private httpClient: HttpClient) {
    this.resetState();
    this.subscriptions.push(this.goPage.subscribe(value => {
      this.state.productsFilter.currentPage = value;
      this.setProducts();
    }));
    this.subscriptions.push(this.sortOptionChange.subscribe(value => {
      this.callGetProducts();
    }));
    this.subscriptions.push(this.filterChange.subscribe(value => {
      this.callGetProducts();
    }));
    this.subscriptions.push(this.categoriesService.selectedCategoryChange.subscribe(value => {
      var filter = this.state.productsFilter;
      this.resetState();
      this.state.productsFilter = filter;
      this.state.productsFilter.categoryId = value;
      this.state.productsFilter.search = null;
      this.callGetProducts();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  resetState() {
    this.state = {
      notFilteredProducts: [],
      allProducts: [],
      products: [],
      productsCount: 0,
      maxPage: 1,
      productsFilter: {
        categoryId: null,
        currentPage: 1,
        pageSize: 15,
        sortOrder: null,
        sizes: [],
        colors: [],
        priceFrom: 0,
        priceTo: 1000,
        search: null
      }
    };
  }

  private setProducts() {
    this.state.products = this.state.allProducts.slice((this.state.productsFilter.currentPage - 1) * this.state.productsFilter.pageSize, this.state.productsFilter.currentPage * this.state.productsFilter.pageSize);
  }

  private callGetProducts() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let url = 'http://localhost:8080/products';
    this.httpClient.post<ProductDTO[]>(url, this.state.productsFilter, httpOptions)
      .subscribe(response => {
        this.state.notFilteredProducts = ProductMapper.map(response);
        this.state.allProducts = this.filterProducts(this.state.notFilteredProducts);
        this.state.productsCount = this.state.allProducts.length;
        this.state.maxPage = Math.ceil(this.state.productsCount / this.state.productsFilter.pageSize);
        this.setProducts();
      });
  }

  filterProducts(products: Product[]): Product[] {
    return products.filter( it => this.filterProduct(it));
  }

  filterProduct(product: Product): boolean {
    return (this.state.productsFilter.colors.length == 0 || product.images.filter(it => this.state.productsFilter.colors.includes(it.color)).length>0)
      && product.price >= this.state.productsFilter.priceFrom
      && product.price <= this.state.productsFilter.priceTo
      && (this.state.productsFilter.search == null || this.state.productsFilter.search.length == 0 || product.name.toUpperCase().includes(this.state.productsFilter.search.toUpperCase()));
  }

  getColors() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let url = 'http://localhost:8080/products/colors';
    return this.httpClient.get<string[]>(url, httpOptions);
  }

}
