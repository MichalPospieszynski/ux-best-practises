import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthorizationService} from "./authorization.service";
import {Router} from "@angular/router";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = AuthorizationService.getToken();

    if (req.headers.has('Authorization')) {
      return next.handle(req);
    }

    const copiedReq = req.clone({headers: req.headers.set('Authorization', token)});
    return next.handle(copiedReq).pipe(catchError(err => {
      if (err.status === 401) {
        this.router.navigate(['/']);
      }
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }

}
