import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ProductsService} from "../../shared/service/products.service";
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-products-filter-modal, [app-products-filter-modal]',
  templateUrl: './products.filter.component.html',
  styleUrls: ['./products.filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsFilterModalComponent implements OnInit{

  state : {
    sizes: string[],
    checkedSizes: string[],
    colors: string[],
    checkedColors: string[],
    priceFrom: number,
    priceTo: number
  };

  options: Options = {
    floor: 0,
    ceil: 1000
  };

  constructor(public activeModal: NgbActiveModal,
              private productsService: ProductsService) {
    this.state = {
      sizes: ['S', 'M', 'L', 'XL'],
      checkedSizes: [],
      colors: [],
      checkedColors: [],
      priceFrom: 0,
      priceTo: 1000
    }

  }

  ngOnInit() {
    this.productsService.getColors()
      .subscribe(colors => {
        this.state.colors = colors;
      }, error => {
        console.log('error status: ' + error)
      });
    this.state.checkedColors = this.productsService.state.productsFilter.colors;
    this.state.checkedSizes = this.productsService.state.productsFilter.sizes;
    this.state.priceFrom = this.productsService.state.productsFilter.priceFrom;
    this.state.priceTo = this.productsService.state.productsFilter.priceTo;
  }

  clickSize(size: string) {
    if (this.state.checkedSizes.includes(size)) {
      this.state.checkedSizes = this.state.checkedSizes.filter(it => it !== size);
    } else {
      this.state.checkedSizes = this.state.checkedSizes.concat(size);
    }
    this.submitFilter();
  }

  clickColor(color: string) {
    if (this.state.checkedColors.includes(color)) {
      this.state.checkedColors = this.state.checkedColors.filter(it => it !== color);
    } else {
      this.state.checkedColors = this.state.checkedColors.concat(color);
    }
    this.submitFilter();
  }

  submitFilter() {
    this.productsService.state.productsFilter.sizes = this.state.checkedSizes;
    this.productsService.state.productsFilter.colors = this.state.checkedColors;
    this.productsService.state.productsFilter.priceFrom = this.state.priceFrom;
    this.productsService.state.productsFilter.priceTo = this.state.priceTo;
    this.productsService.filterChange.next();
    /*this.activeModal.close();*/
  }

}
