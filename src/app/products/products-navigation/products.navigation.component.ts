import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Category} from "../../model/shop/Category";
import {Subscription} from "rxjs";
import {CategoryHelper} from "../../shared/utils/category.helper";
import {CategoriesService} from "../../shared/service/categories.service";
import {ProductsService} from "../../shared/service/products.service";

@Component({
  selector: 'app-products-navigation, [app-products-navigation]',
  templateUrl: './products.navigation.component.html',
  styleUrls: ['./products.navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsNavigationComponent implements OnInit, OnDestroy {

  @Output() changeSelectedCategory = new EventEmitter<number>();

  private subscriptions: Subscription[] = [];

  state: {
    selected: Category,
    categories: Category[]
  };

  constructor(public categoriesService: CategoriesService,
              public productsService: ProductsService,
              public categoryHelper: CategoryHelper) {
    this.state = {
      selected: null,
      categories: []
    }
  }

  ngOnInit(): void {
    this.getCategories();
    this.subscriptions.push(this.categoriesService.mainCategoryChange.subscribe(value => {
      this.getCategories();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  getCategories() {
    this.state.categories = this.categoriesService.getSubCategories();
  }

  changeCategoryVisibility(id: number) {
    this.productsService.state.productsFilter.search = null;
    let category = this.categoryHelper.getCategory(this.state.categories, id);
    if (category != null) {
      category.visible = !category.visible;
      if (category.visible && category.parent != null) {
        let otherVisibleSubcategory = category.parent.subCategories.find(subCat => subCat.id != category.id && subCat.visible);
        if (otherVisibleSubcategory != null) {
          otherVisibleSubcategory.visible = false;
        }
      }
      if (category.visible && category.parent == null) {
        let otherVisibleCategory = this.state.categories.find(c => c.id != category.id && c.visible);
        if (otherVisibleCategory != null) {
          otherVisibleCategory.subCategories.forEach(sc => sc.visible = false);
          otherVisibleCategory.visible = false;
        }
      }
      if (!category.visible && category.parent == null) {
        category.subCategories.forEach(c => c.visible = false);
      }
      this.state.selected = category.visible ? category : (category.parent == null ? null : category.parent);
    }
    this.changeSelectedCategory.emit(this.state.selected ? this.state.selected.id : null);
  }

}

