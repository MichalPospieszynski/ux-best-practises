import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Category} from "../model/shop/Category";
import {ActivatedRoute} from "@angular/router";
import {isNullOrUndefined} from "util";
import {Subscription} from "rxjs";
import {CategoriesService} from "../shared/service/categories.service";

@Component({
  selector: 'app-products, [app-products]',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {

  state : {
    selectedCategoryId: number
  };

  constructor(private categoriesService: CategoriesService,
              private route: ActivatedRoute) {
    this.state = {
      selectedCategoryId: null
    }
  }

  ngOnInit() {
    this.loadParam();
    this.route.params.subscribe(() => {
      const value: number = Number(this.route.snapshot.paramMap.get('categoryId'));
      this.categoriesService.selectedCategoryChange.next(value);
      this.state.selectedCategoryId = value;
      this.categoriesService.mainCategoryChange.next(value);
    });
  }

  loadParam() {
    let categoryId: number = Number(this.route.snapshot.paramMap.get('categoryId'));
    this.changeSelectedCategory(categoryId);
    this.categoriesService.state.mainCategory = isNullOrUndefined(categoryId) ? null : categoryId;
  }

  changeSelectedCategory(categoryId: number) {
    this.categoriesService.selectedCategoryChange.next(categoryId);
    this.state.selectedCategoryId = categoryId;
  }

}

