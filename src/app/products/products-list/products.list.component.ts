import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {ProductsService} from "../../shared/service/products.service";
import {CategoriesService} from "../../shared/service/categories.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {isNullOrUndefined} from "util";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ProductsFilterModalComponent} from "../products-filter/products.filter.component";
import {ProductsFilter} from "../../model/shop/ProductsFilter";

const SortOption = {
  PRICE_LOW_TO_HIGH: "Cena rosnąco",
  PRICE_HIGH_TO_LOW: "Cena malejąco",
  ALPHABET_ASC: "Alfabetycznie od A do Z",
  ALPHABET_DESC: "Alfabetycznie od Z do A",
};

@Component({
  selector: 'app-products-list, [app-products-list]',
  templateUrl: './products.list.component.html',
  styleUrls: ['./products.list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsListComponent implements OnChanges, OnInit, OnDestroy {

  @Input() selectedCategoryId: number;

  private subscriptions: Subscription[] = [];

  state: {
    breadcrumbs: string[],
    categoryId: number,
    categoryName: string,
    sortOptions: string[],
    sortOption: string,
    pagination: {
      page: number,
      prevPage: number,
      nextPage: number
    }
  };

  constructor(public productsService: ProductsService,
              private modalService: NgbModal,
              public categoriesService: CategoriesService,
              private router: Router) {
    this.resetState();
  }

  resetState() {
    this.state = {
      breadcrumbs: [],
      categoryId: null,
      categoryName: null,
      sortOptions: [
        SortOption.PRICE_LOW_TO_HIGH,
        SortOption.PRICE_HIGH_TO_LOW,
        SortOption.ALPHABET_ASC,
        SortOption.ALPHABET_DESC
      ],
      sortOption: SortOption.PRICE_LOW_TO_HIGH,
      pagination: {
        page: 2,
        prevPage: 1,
        nextPage: 3
      }
    }
  }

  ngOnInit() {
    if (isNullOrUndefined(this.selectedCategoryId)) {
      this.router.navigate(['/']);
    }
    this.subscriptions.push(this.categoriesService.mainCategoryChange.subscribe(value => {
      this.resetState();
    }));
    this.setBreadcrumbs();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges) {
    this.state.categoryId = changes.selectedCategoryId.currentValue;
    if (this.state.categoryId != null) {
      let category = this.categoriesService.getCategoryById(this.state.categoryId);
      this.state.categoryName = isNullOrUndefined(category) ? "" : category.name;
    }
    this.setBreadcrumbs();
  }

  setBreadcrumbs() {
    if(this.state.categoryId != null) {
      let bc = [];
      let cat = this.categoriesService.getCategoryById(this.state.categoryId);
      bc.push(cat.name);
      if(cat.parent != null) {
        let par = cat.parent;
        bc.push(par.name);
        if(par.parent != null) {
          bc.push(par.parent.name);
        }
      }
      this.state.breadcrumbs = bc.reverse();
    }
  }

  nextPage() {
    if (this.productsService.state.productsFilter.currentPage < this.productsService.state.maxPage) {
      this.productsService.goPage.next(this.productsService.state.productsFilter.currentPage + 1);
      this.setPaginationLabels();
    }
  }

  prevPage() {
    if (this.productsService.state.productsFilter.currentPage > 1) {
      this.productsService.goPage.next(this.productsService.state.productsFilter.currentPage - 1);
      this.setPaginationLabels();
    }
  }

  goPage(page: number) {
    if (page <= this.productsService.state.maxPage && page > 0) {
      this.productsService.goPage.next(page);
      this.setPaginationLabels();
    }
  }

  setPaginationLabels() {
    this.state.pagination.prevPage = this.productsService.state.productsFilter.currentPage == 1 ? 1 : (this.productsService.state.productsFilter.currentPage == this.productsService.state.maxPage ? this.productsService.state.productsFilter.currentPage - 2 : this.productsService.state.productsFilter.currentPage - 1);
    this.state.pagination.page = this.productsService.state.productsFilter.currentPage == 1 ? 2 : (this.productsService.state.productsFilter.currentPage == this.productsService.state.maxPage ? this.productsService.state.productsFilter.currentPage - 1 : this.productsService.state.productsFilter.currentPage);
    this.state.pagination.nextPage = this.productsService.state.productsFilter.currentPage == 1 ? 3 : (this.productsService.state.productsFilter.currentPage == this.productsService.state.maxPage ? this.productsService.state.productsFilter.currentPage : this.productsService.state.productsFilter.currentPage + 1);
  }

  changeSortOption() {
    this.productsService.state.productsFilter.sortOrder = this.state.sortOption;
    this.productsService.sortOptionChange.next();
  }

  openFilterModal() {
    this.modalService.open(ProductsFilterModalComponent, {windowClass: "products-filter-modal"});
  }

  removeColorFilter(color: string) {
    this.productsService.state.productsFilter.colors = this.productsService.state.productsFilter.colors.filter(it => it != color);
    this.productsService.filterChange.next();
  }

  removeSizeFilter(size: string) {
    this.productsService.state.productsFilter.sizes = this.productsService.state.productsFilter.sizes.filter(it => it != size);
    this.productsService.filterChange.next();
  }
}

