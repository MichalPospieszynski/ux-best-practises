export class Category {

  id : number;
  name : string;
  visible : boolean;
  parent : Category;
  subCategories : Category[];

  constructor(id : number ,name : string){
    this.id = id;
    this.name = name;
    this.visible = false;
    this.parent = null;
    this.subCategories = [];
  }

}
