export class ProductsFilter {

  categoryId: number;
  currentPage: number;
  pageSize: number;
  sortOrder: string;
  sizes: string[];
  colors: string[];
  priceFrom: number;
  priceTo: number;
  search: string;

}
