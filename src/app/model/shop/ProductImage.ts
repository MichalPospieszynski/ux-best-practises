export class ProductImage {

  order: number;
  color: string;
  content: File;

  constructor(order: number, color: string, content: File) {
    this.order = order;
    this.color = color;
    this.content = content;
  }

}
