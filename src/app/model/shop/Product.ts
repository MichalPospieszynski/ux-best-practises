import {ProductImage} from "./ProductImage";

export class Product {

  id: number;
  name: string;
  style: string;
  fabric: string;
  fashion: string;
  brandLogo: File;
  price: number;
  images: ProductImage[];
  categoryId: number;

  constructor(id: number, name: string, style: string, fabric: string, fashion: string, brandLogo: File, price: number, images: ProductImage[], categoryId: number) {
    this.id = id;
    this.name = name;
    this.style = style;
    this.fabric = fabric;
    this.fashion = fashion;
    this.brandLogo = brandLogo;
    this.price = price;
    this.images = images;
    this.categoryId = categoryId;
  }

}
