import {ProductImageDTO} from "./ProductImageDTO";

export class ProductDTO {

  id : number;
  name : string;
  style : string;
  fabric : string;
  fashion : string;
  brandLogo : File;
  price : number;
  images : ProductImageDTO[];
  categoryId : number;

}
