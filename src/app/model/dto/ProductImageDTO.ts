export class ProductImageDTO {

  order: number;
  color: string;
  content: File;

}
