export class CategoryDTO {

  id : number;
  name : string;
  visible : boolean;
  parentId : number;

}
