import {ProductDTO} from "./ProductDTO";

export class CartItemDTO {

  product: ProductDTO;
  size: string;
  amount: number;

}
