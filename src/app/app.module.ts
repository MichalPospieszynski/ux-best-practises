import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from "./header/header.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProductsComponent} from "./products/products.component";
import {HomeComponent} from "./home/home.component";
import {ProductsListComponent} from "./products/products-list/products.list.component";
import {ProductsNavigationComponent} from "./products/products-navigation/products.navigation.component";
import {FooterComponent} from "./footer/footer.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./shared/service/auth.interceptor";
import {UserModalComponent} from "./user/user.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductDetailsComponent} from "./product-details/product.details.component";
import {ProductsFilterModalComponent} from "./products/products-filter/products.filter.component";
import {Ng5SliderModule} from 'ng5-slider';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductsComponent,
    ProductDetailsComponent,
    ProductsListComponent,
    ProductsNavigationComponent,
    UserModalComponent,
    ProductsFilterModalComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    Ng5SliderModule,
    BrowserAnimationsModule,
    MatSnackBarModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 1000}}
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    UserModalComponent,
    ProductsFilterModalComponent
  ]
})
export class AppModule {
}
