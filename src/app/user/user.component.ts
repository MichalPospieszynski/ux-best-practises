import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthorizationService} from "../shared/service/authorization.service";

@Component({
  selector: 'app-user-modal, [app-user-modal]',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserModalComponent implements OnInit{

  signinForm: FormGroup;
  signupForm: FormGroup;
  emailCtrl: FormControl;
  passwordCtrl: FormControl;

  constructor(public activeModal: NgbActiveModal,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.emailCtrl = new FormControl('user@aa.aa', [
      Validators.required,
      Validators.email,
      Validators.maxLength(100)
    ]);

    this.passwordCtrl = new FormControl('password', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(100)
    ]);

    this.signinForm = new FormGroup({
      email: this.emailCtrl,
      password : this.passwordCtrl
    });

    this.signupForm = new FormGroup({
      email: this.emailCtrl,
      password : this.passwordCtrl
    });
  }

  onSingupSubmit() {
    let email = this.signinForm.get('email').value;
    let password = this.signinForm.get('password').value;
    this.authorizationService.signUp(email, password);
  }

  onSinginSubmit() {
    let email = this.signinForm.get('email').value;
    let password = this.signinForm.get('password').value;
    this.authorizationService.signIn(email, password)
      .subscribe(token =>{
        localStorage.setItem('token', 'bearer ' +token.access_token);
        this.authorizationService.email = email;
        this.activeModal.close();
      });
  }

}
